﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorHelper
{
    internal static class Global
    {
        public static string ApplicationName { get; set; }
        public static bool Debug { get; set; }
        public static class Email
        {
            public static bool Log { get; set; }

            public static string SMTPServer { get; set; }
            public static string From { get; set; }
            public static List<string> To { get; set; }
            public static List<string> CC { get; set; }
            public static List<string> BCC { get; set; }
        }
        public static class Errors
        {
            public static ErrorHandlingEnums.Severity LogLevel { get; set; }
        }
    }
}
