﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ErrorHelper
{
    /// <summary>
    /// Enumerações usadas pela biblioteca
    /// </summary>
    public static class ErrorHandlingEnums
    {
        /// <summary>
        /// Lista de severidades dos erros
        /// </summary>
        public enum Severity
        {
            Info = 0,
            Warning = 1,
            Error = 2,
            Critical = 3
        }
    }
}