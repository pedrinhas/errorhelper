﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace ErrorHelper
{
    internal static class Tools
    {
        public static class Email
        {
            public static bool SendEmail(List<string> to, string subject, string body, List<string> cc = null, List<string> bcc = null)
            {
                try
                {
                    return SendEmail(Global.Email.SMTPServer, Global.Email.From, to, subject, body, cc, bcc);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            private static bool SendEmail(string stmp, string from, List<string> to, string subject, string body, List<string> cc = null, List<string> bcc = null)
            {
                try
                {
                    MailMessage mail = new MailMessage(from, to.First());
                    foreach (var toAddress in to.Where(x => x != to.First()))
                    {
                        mail.To.Add(toAddress);
                    }
                    foreach (var ccAddress in cc)
                    {
                        mail.CC.Add(ccAddress);
                    }
                    foreach (var bccAddress in bcc)
                    {
                        mail.Bcc.Add(bccAddress);
                    }
                    SmtpClient client = new SmtpClient();
                    client.Port = 25;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Host = stmp;
                    mail.Subject = subject.Replace('\n', ' ').Replace('\r', ' ');
                    mail.Body = body;
                    client.Send(mail);
                }
                catch (Exception ex)
                {
                    return false;
                }

                return true;
            }
        }
    }
}