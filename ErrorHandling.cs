﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ErrorHelper
{
    /// <summary>
    /// Envio de logs de exceções por email e base de dados
    /// </summary>
    public static class ErrorHandling
    {
        private static bool initialized = false;
        /// <summary>
        /// Inicializa a biblioteca. É necessário para definir parâmetros como destinários dos logs, endereço SMTP e log level mínimo
        /// </summary>
        /// <param name="appName">Nome da aplicação que usa a biblioteca</param>
        /// <param name="debug">Flag de debug. Não usada de momento</param>
        /// <param name="smtp">Endereço SMTP para enviar emails</param>
        /// <param name="from">Email a aparecer como remetente</param>
        /// <param name="to">Lista de destinarários dos emails de log</param>
        /// <param name="cc">Lista de CC dos emails de log</param>
        /// <param name="bcc">Lista de Bcc dos emails de log</param>
        /// <param name="minimumSeverity">Severidade mínima necessária para enviar um email</param>
        public static void Initialize(string appName, bool debug, string smtp, string from, IEnumerable<string> to, IEnumerable<string> cc, IEnumerable<string> bcc, ErrorHandlingEnums.Severity minimumSeverity)
        {
            Global.ApplicationName = appName;
            Global.Debug = debug;
            Global.Email.SMTPServer = smtp;
            Global.Email.From = from;
            Global.Email.To = to.ToList();
            Global.Email.CC = cc.ToList();
            Global.Email.BCC = bcc.ToList();

            if (minimumSeverity > ErrorHandlingEnums.Severity.Critical)
                Global.Errors.LogLevel = ErrorHandlingEnums.Severity.Critical;
            else if (minimumSeverity < ErrorHandlingEnums.Severity.Info)
                Global.Errors.LogLevel = ErrorHandlingEnums.Severity.Info;
            else Global.Errors.LogLevel = minimumSeverity;

            initialized = true;
            Global.Email.Log = true;
        }

        /// <summary>
        /// Enviar log de uma exceção com um objeto embutido
        /// </summary>
        /// <param name="ex">Exceção a enviar</param>
        /// <param name="obj">Objeto a registar</param>
        /// <param name="severity">Severidade do erro</param>
        /// <returns></returns>
        public static bool LogError(Exception ex, Object obj = null, ErrorHandlingEnums.Severity severity = ErrorHandlingEnums.Severity.Error)
        {
            if (severity < Global.Errors.LogLevel) return false;
            if (!initialized) return false;

            bool email = false, db = false;
            Exception exObj = null;

            if(obj != null)
            {
                exObj = new Exception(new JavaScriptSerializer().Serialize(obj), ex);
            }

            if (Global.Email.Log) email = LogErrorEmail(exObj ?? ex, severity);
            else email = true; //caso os logs para email estiverem desligados, não dá false só porque não mandou email
            //db = LogErrorDB(exObj ?? ex, severity);

            //return email && db;
            return email;
        }

        /// <summary>
        /// Enviar log de uma exceção por email com um assunto e corpo de mensagem personalizados
        /// </summary>
        /// <param name="to">Lista de destinatários do log</param>
        /// <param name="subject">Assunto do email</param>
        /// <param name="body">Corpo do email</param>
        /// <param name="ex">Exceção a enviar</param>
        /// <param name="severity">Severidade do erro</param>
        /// <param name="cc">Lista de CC</param>
        /// <param name="bcc">Lista de Bcc</param>
        /// <returns></returns>
        public static bool LogErrorEmail(List<string> to, string subject, string body, Exception ex, ErrorHandlingEnums.Severity severity = ErrorHandlingEnums.Severity.Error, List<string> cc = null, List<string> bcc = null)
        {
            if (severity < Global.Errors.LogLevel) return false;
            if (!initialized) return false;

            if (!string.IsNullOrWhiteSpace(body)) body = body.TrimEnd(new char[] { '\n', '\r' }) + string.Format("{0}{0}", Environment.NewLine);

            body += string.Format("IP{0}{1}{2}{0}{0}", Environment.NewLine, "\t", HttpContext.Current.Request.UserHostAddress);
            body += string.Format("URL{0}{1}{2}{0}{0}", Environment.NewLine, "\t", HttpContext.Current.Request.Url);
            body += string.Format("Http Status Code{0}{1}{2} - {3}{0}{0}", Environment.NewLine, "\t", HttpContext.Current.Response.StatusCode + "." + HttpContext.Current.Response.SubStatusCode, HttpContext.Current.Response.StatusDescription);

            body += string.Format("Request Headers{0}", Environment.NewLine);

            foreach (var header in HttpContext.Current.Request.Headers.AllKeys)
            {
                var val = HttpContext.Current.Request.Headers[header];

                body += string.Format("{1}{2}: {3}{0}", Environment.NewLine, "\t", header, val);
            }

            body += Environment.NewLine;

            body += string.Format("Response Headers{0}", Environment.NewLine);

            foreach (var header in HttpContext.Current.Response.Headers.AllKeys)
            {
                var val = HttpContext.Current.Response.Headers[header];

                body += string.Format("{1}{2}: {3}{0}", Environment.NewLine, "\t", header, val);
            }


            string exText = "";

            do
            {
                exText += string.Format("{0} - {1}", ex.GetType().ToString(), ex.Message);

                if (!string.IsNullOrWhiteSpace(ex.StackTrace)) exText += string.Format("{0}{1}StackTrace:{0}{1}{2}", Environment.NewLine, "\t", ex.StackTrace);
                if (ex.TargetSite != null && !string.IsNullOrWhiteSpace(ex.TargetSite.ToString())) exText += string.Format("{0}{0}{1}TargetSite:{0}{1}{2}{3}{3}", Environment.NewLine, "\t", ex.TargetSite, ex.InnerException == null ? "" : Environment.NewLine);

                ex = ex.InnerException;
            } while (ex != null);

            var bodyAll = "";

            if (string.IsNullOrWhiteSpace(body))
            {
                bodyAll = string.Format("Exceptions{0}{1}{2}", Environment.NewLine, "\t", exText);
            }
            else
            {
                bodyAll = string.Format("{2}{0}{0}Exceptions{0}{1}{3}", Environment.NewLine, "\t", body.TrimEnd(new char[] { '\n', '\r' }), exText);
            }

            return Tools.Email.SendEmail(to, subject, bodyAll, cc, bcc);
        }
        /// <summary>
        /// Enviar log de uma exceção por email
        /// </summary>
        /// <param name="to">Lista de destinatários do log</param>
        /// <param name="ex">Exceção a enviar</param>
        /// <param name="severity">Severidade do erro</param>
        /// <param name="cc">Lista de CC</param>
        /// <param name="bcc">Lista de Bcc</param>
        /// <returns></returns>
        public static bool LogErrorEmail(List<string> to, Exception ex, ErrorHandlingEnums.Severity severity = ErrorHandlingEnums.Severity.Error, List<string> cc = null, List<string> bcc = null)
        {
            if (severity < Global.Errors.LogLevel) return false;
            if (!initialized) return false;

            string subject = string.Format("[{2}] [{3}] Exception: {0} - {1}", ex.GetType().ToString(), ex.Message, severity.ToString(), Global.ApplicationName);

            return LogErrorEmail(to, subject, "", ex, severity, cc, bcc);
        }
        /// <summary>
        /// Enviar log de uma exceção por email
        /// </summary>
        /// <param name="ex">Exceção a enviar</param>
        /// <param name="severity">Severidade do erro</param>
        /// <returns></returns>
        public static bool LogErrorEmail(Exception ex, ErrorHandlingEnums.Severity severity = ErrorHandlingEnums.Severity.Error)
        {
            if (severity < Global.Errors.LogLevel) return false;
            if (!initialized) return false;

            return LogErrorEmail(Global.Email.To, ex, severity, Global.Email.CC, Global.Email.BCC);
        }

        /// <summary>
        /// Armazenar log na base de dados
        /// </summary>
        /// <param name="ex">Exceção a registar</param>
        /// <param name="severity">Severidade do erro</param>
        /// <returns></returns>
        [Obsolete("Ainda não implementado", true)]
        public static bool LogErrorDB(Exception ex, ErrorHandlingEnums.Severity severity = ErrorHandlingEnums.Severity.Error)
        {
            if (severity < Global.Errors.LogLevel) return false;
            if (!initialized) return false;

            return true;
        }
    }
}